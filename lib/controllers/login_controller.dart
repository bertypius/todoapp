import 'package:todoapp/utils/database_helper.dart';

import '../models/user.dart';

class Login {
  final _helper = DatabaseHelper();
  String username = "";

  Future<String> verifyUser(User data) async {
    var user = await _helper.verifyUserData(data);
    if (user != null) {
      username = user.name!;
    }
    return username;
  }

  String? validateUserEmail(String? data) {
    if (data!.isEmpty) {
      return "This field is required";
    } else if (!RegExp(r'(^[\w-\.]+@([\w-]+\.)+[\w]{2,5}$)').hasMatch(data)) {
      return "Enter a valid email address";
    } else {
      return null;
    }
  }

  String? validateUserPassword(String? data) {
    if (data!.isEmpty) {
      return "This field is required";
    } else if (data.length < 6) {
      return "Your password should be at least 6 characters";
    } else {
      return null;
    }
  }

  String? validateUserPasswordMatch() {
    if (username.isEmpty) {
      return "The username and password do not match";
    } else {
      return null;
    }
  }
}
