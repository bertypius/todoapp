import '../resources/includes.dart';

@override
Widget drawerWidget(BuildContext context) {
  Size size = MediaQuery.of(context).size;
  var drawerHeaderHeight = size.height * 0.28;
  TextStyle textStyle = const TextStyle(fontWeight: FontWeight.w300);

  return Drawer(
    backgroundColor: Colors.orange,
    child: SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: drawerHeaderHeight,
            child: DrawerHeader(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CircleAvatar(
                      radius: size.height * 0.05,
                      backgroundImage: const AssetImage(
                        "assets/images/profile/profile.jpg",
                      )),
                  const Spacer(),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Column(
                      children: const [
                        Text(
                          "PREMIUM USER",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              letterSpacing: 4.0,
                              wordSpacing: 4.0,
                              fontSize: 15.0),
                        ),
                        Text(
                          "Developedby-| gilberypius05@gmail.com",
                          style: TextStyle(
                              color: Colors.white,
                              height: 1.8,
                              fontWeight: FontWeight.w300,
                              fontSize: 12.0),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            color: Colors.white,
            height: size.height - drawerHeaderHeight,
            child: ListView(
              padding: const EdgeInsets.only(top: 15),
              children: [
                ListTile(
                  onTap: () {},
                  title: Text("Home", style: textStyle),
                  leading: const Icon(
                    Icons.home,
                    color: btnColor,
                  ),
                ),
                ListTile(
                  title: Text("Friends and Family", style: textStyle),
                  leading: const Icon(
                    Icons.people_alt_outlined,
                    color: btnColor,
                  ),
                ),
                ListTile(
                  title: Text("Categories", style: textStyle),
                  leading: const Icon(
                    Icons.category_outlined,
                    color: btnColor,
                  ),
                ),
                ListTile(
                  title: Text("Share this App", style: textStyle),
                  leading: const Icon(
                    Icons.share,
                    color: btnColor,
                  ),
                ),
                ListTile(
                  title: Text("Get more Apps", style: textStyle),
                  leading: const Icon(
                    Icons.search_outlined,
                    color: btnColor,
                  ),
                ),
                ListTile(
                  title: Text("Feedback", style: textStyle),
                  leading: const Icon(
                    Icons.feedback_outlined,
                    color: btnColor,
                  ),
                ),
                ListTile(
                  title: Text("Settings", style: textStyle),
                  leading: const Icon(
                    Icons.settings,
                    color: btnColor,
                  ),
                ),
                const SizedBox(height: 50.0),
                const Divider(height: 0),
                ListTile(
                  onTap: (() {
                    showDialog(
                      context: context,
                      builder: ((context) {
                        return AlertDialog(
                          contentPadding: const EdgeInsets.only(
                              top: 20.0, left: 15.0, right: 5.0, bottom: 10),
                          content: const Text(
                            textAlign: TextAlign.center,
                            "Are you sure want to log out ?",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w300),
                          ),
                          actionsPadding: const EdgeInsets.symmetric(
                              horizontal: 15, vertical: 10),
                          actions: [
                            TextButton(
                              onPressed: (() {
                                Navigator.pop(context);
                              }),
                              child: Text("Cancel",
                                  style: TextStyle(
                                    color: Colors.black.withOpacity(0.8),
                                    fontSize: 17,
                                  )),
                            ),
                            TextButton(
                              onPressed: (() {
                                Navigator.pop(context);
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) =>
                                            const AuthScreenToggler()));
                              }),
                              child: const Text(
                                "Confirm",
                                style: TextStyle(
                                  color: btnColor,
                                  fontSize: 17,
                                ),
                              ),
                            )
                          ],
                        );
                      }),
                    );
                  }),
                  title: const Text("Log Out"),
                  leading: const Icon(
                    Icons.exit_to_app_rounded,
                    color: btnColor,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    ),
  );
}
