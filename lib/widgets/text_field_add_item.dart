import '../resources/includes.dart';

taskTextField({
  required BuildContext context,
  required GlobalKey<FormFieldState> formKey,
  required TextEditingController newItemController,
  required Function saveTodoItem,
  Todo? item,
  bool updateItem = false,
}) {
  if (item != null && item.title != null) {
    newItemController.text = item.title!;
  }
  return showModalBottomSheet(
    barrierColor: Colors.transparent,
    backgroundColor: Colors.transparent,
    isScrollControlled: true,
    isDismissible: false,
    context: context,
    builder: ((context) {
      return Container(
        padding: EdgeInsets.only(
          top: 20,
          left: 25,
          right: 25,
          bottom: MediaQuery.of(context).viewInsets.bottom + 30,
        ),
        decoration: BoxDecoration(
          color: bgColor,
          boxShadow: [
            BoxShadow(
              blurRadius: 25,
              offset: const Offset(-1, -1),
              color: Colors.black.withOpacity(.2),
            )
          ],
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(40),
            topRight: Radius.circular(40),
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Flexible(
              child: Text(
                "Add new task",
                style: TextStyle(
                    fontSize: 27,
                    fontWeight: FontWeight.w600,
                    color: Colors.green),
              ),
            ),
            const SizedBox(height: 15),
            Flexible(
              child: Form(
                child: TextFormField(
                  key: formKey,
                  controller: newItemController,
                  cursorColor: Colors.black12,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Enter a value before submitting!";
                    }
                    return null;
                  },
                  maxLines: 3,
                  decoration: const InputDecoration(
                    hintText: "Add your new task here...",
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  const Spacer(flex: 3),
                  TextButton(
                    onPressed: () {
                      newItemController.clear();
                      Navigator.pop(context);
                    },
                    style: TextButton.styleFrom(
                      backgroundColor: btnColor,
                    ),
                    child: const Text(
                      "Cancel",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  const Spacer(flex: 1),
                  TextButton(
                    onPressed: () {
                      if (item != null && updateItem == true) {
                        saveTodoItem(context, updateItem, item);
                      } else {
                        saveTodoItem(context);
                      }
                    },
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.green,
                    ),
                    child: const Text(
                      "Save",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );
    }),
  );
}
