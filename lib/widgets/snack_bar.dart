import '../resources/includes.dart';

void showSnackBar(BuildContext context, String message, [int duration = 4]) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      backgroundColor: btnColor,
      content: Text(message),
      duration: Duration(seconds: duration),
    ),
  );
}
