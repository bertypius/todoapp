import '../resources/includes.dart';

Widget searchBar({
  required BuildContext context,
  required Function filterTodos,
}) {
  return SizedBox(
    height: MediaQuery.of(context).size.height * 0.1,
    child: Stack(
      children: [
        Container(
          height: MediaQuery.of(context).size.height * 0.1 - 22,
          decoration: const BoxDecoration(
            color: Colors.orange,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25),
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
            child: Container(
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                  blurRadius: 20,
                  color: Colors.black.withOpacity(0.13),
                  offset: const Offset(1, 4),
                )
              ]),
              child: TextField(
                onChanged: (value) {
                  filterTodos(value);
                },
                cursorColor: Colors.black12,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(vertical: 0),
                  hintText: "Search",
                  prefixIcon: const Icon(
                    Icons.search,
                    size: 20,
                    color: Colors.black54,
                  ),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: BorderSide.none),
                  fillColor: Colors.white,
                  filled: true,
                ),
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
