import '../resources/includes.dart';

class CustomWillPopScope extends StatefulWidget {
  final Widget child;
  const CustomWillPopScope({super.key, required this.child});

  @override
  State<CustomWillPopScope> createState() => _CustomWillPopScope();
}

class _CustomWillPopScope extends State<CustomWillPopScope> {
  Future<bool> _onBackPress() async {
    bool exitApp = await showDialog(
      context: context,
      builder: ((context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.only(
              top: 20.0, left: 15.0, right: 5.0, bottom: 10),
          content: const Text(
            textAlign: TextAlign.center,
            "Are you sure want to exit the App ?",
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.w300),
          ),
          actionsPadding:
              const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          actions: [
            TextButton(
              onPressed: (() {
                Navigator.pop(context, false);
              }),
              child: Text("No",
                  style: TextStyle(
                    color: Colors.black.withOpacity(0.8),
                    fontSize: 17,
                  )),
            ),
            TextButton(
              onPressed: (() {
                Navigator.pop(context, true);
              }),
              child: const Text(
                "Yes",
                style: TextStyle(
                  color: btnColor,
                  fontSize: 17,
                ),
              ),
            )
          ],
        );
      }),
    );
    return exitApp;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPress,
      child: widget.child,
    );
  }
}
