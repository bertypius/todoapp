import '../resources/includes.dart';

AppBar appBar(String username) {
  return AppBar(
    systemOverlayStyle: const SystemUiOverlayStyle(
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness: Brightness.dark,
      statusBarColor: btnColor,
    ),
    elevation: 0,
    backgroundColor: Colors.orange,
    title: Text(username, style: const TextStyle(fontWeight: FontWeight.w300)),
    centerTitle: true,
    actions: const [
      Padding(
        padding: EdgeInsets.only(right: 20.0),
        child: CircleAvatar(
          backgroundColor: Colors.orange,
          backgroundImage: AssetImage("assets/images/profile/profile.jpg"),
        ),
      )
    ],
  );
}
