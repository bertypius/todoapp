import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../models/todo.dart';
import '../models/user.dart';

class DatabaseHelper {
  static final DatabaseHelper instance = DatabaseHelper._instance();
  DatabaseHelper._instance();
  Database? _database;
  factory DatabaseHelper() {
    return instance;
  }
  Future<Database> get database async {
    instance._database ??= await initDatabase();
    return instance._database!;
  }

  Future<Database> initDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    var path = " ${directory.path}todoApp.db";
    // debugPrint(await getDatabasesPath());
    var todoDatabase =
        await openDatabase(path, version: 1, onCreate: _onCreatingDb);
    return todoDatabase;
  }

  void _onCreatingDb(Database db, int version) async {
    await db.execute(
      'CREATE TABLE IF NOT EXISTS todos (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, status INTEGER)',
    );
    await db.execute(
      'CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, email TEXT,password TEXT)',
    );
  }

// -----------User Model data------------/
//  create user
  Future<int> createUser(User user) async {
    var db = await instance.database;
    var result = await db.insert("users", user.toMap());
    return result;
  }

// get user data
  Future<List<User>> getUserData(int id) async {
    var db = await instance.database;
    List<Map<String, dynamic>> result =
        await db.query("users", where: "id=?", whereArgs: ["$id"]);
    List<User> user = result.map((mapObject) {
      return User.fromMap(mapObject);
    }).toList();
    return user;
  }

  Future<User?> verifyUserData(User findUser) async {
    List<User> userList = [];
    User? user;
    var db = await instance.database;
    var result = await db.rawQuery(
        "SELECT * FROM users WHERE email='${findUser.email}' and password='${findUser.password}'");
    if (result.isNotEmpty) {
      userList = result.map((mapObject) {
        return User.fromMap(mapObject);
      }).toList();
      for (user in userList) {
        return user;
      }
    }
    return user;
  }

// get a list of users
  Future<List<User>> getUsers(User user) async {
    var db = await instance.database;
    List<Map<String, dynamic>> result = await db.query("users");
    List<User> usersList = result.map((mapObject) {
      return User.fromMap(mapObject);
    }).toList();
    return usersList;
  }

  // delete user
  Future<int> deleteUser(int? id) async {
    var db = await instance.database;
    int result;
    if (id != null) {
      result = await db.delete("users", where: "id=?", whereArgs: ["$id"]);
    } else {
      result = await db.delete("users");
    }
    return result;
  }

// update user
  Future<int> updateUser(User user) async {
    var db = await instance.database;
    int result = await db.update("users", user.toMap(),
        where: "id=?", whereArgs: ["${user.id}"]);
    return result;
  }

// -----------Todo Model data------------/
//  create new task(todo item)
  Future<int> insertTodoItem(Todo todo) async {
    var db = await instance.database;
    var result = await db.insert(
        "todos", conflictAlgorithm: ConflictAlgorithm.replace, todo.toMap());
    return result;
  }

// get todo item
  Future<List<Todo>> getTodoItems(Todo todo) async {
    var db = await instance.database;
    List<Map<String, dynamic>> result = await db.query("todos");
    List<Todo> todoList = result.map((mapObject) {
      return Todo.fromMap(mapObject);
    }).toList();
    return todoList;
  }

  // delete todo item
  Future<int> deleteTodoItem(int? id) async {
    var db = await instance.database;
    int result;
    if (id != null) {
      result = await db.delete("todos", where: "id=?", whereArgs: ["$id"]);
    } else {
      result = await db.delete("todos");
    }
    return result;
  }

// update todo item
  Future<int> updateTodoItem(Todo todo) async {
    var db = await instance.database;
    int result = await db.update("todos", todo.toMap(),
        where: "id=?", whereArgs: ["${todo.id}"]);
    return result;
  }
}
