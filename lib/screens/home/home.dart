import '../../resources/includes.dart';

class HomeScreen extends StatefulWidget {
  final String username;
  const HomeScreen(this.username, {super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool isVisible = false;
  bool isAddTodoToggled = false;
  final _todoItemController = TextEditingController();
  final DatabaseHelper _helper = DatabaseHelper();
  List<Todo> searchTodo = [];
  String? username;
  final _todoItemKey = GlobalKey<FormFieldState>();

  void _setOnboardingValue() async {
    var pref = await SharedPreferences.getInstance();
    await pref.setBool("seenOnboarding", true);
  }

  @override
  void initState() {
    super.initState();
    updateTodoList();
    // getUserData(User());
    _setOnboardingValue();
  }

  @override
  void dispose() {
    super.dispose();
    _todoItemController.dispose();
  }

  Todo todo = Todo();
  bool isCompleted = true;
  List<Todo> todoList = [];

// search todo items
  void filterTodos(String enteredkeyword) {
    List<Todo> result = [];
    if (enteredkeyword.isEmpty) {
      result = todoList;
    } else {
      result = todoList
          .where((item) => item.title!.toLowerCase().contains(
                enteredkeyword.toLowerCase(),
              ))
          .toList();
    }
    setState(() {
      searchTodo = result;
      isVisible = !isVisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return CustomWillPopScope(
      child: Scaffold(
        backgroundColor: bgColor,
        appBar: appBar(widget.username),
        drawer: drawerWidget(context),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            searchBar(context: context, filterTodos: filterTodos),
            const SizedBox(height: 25),
            titleWithButton(context),
            Expanded(
              child: ListView.builder(
                itemCount: searchTodo.length,
                itemBuilder: ((context, index) {
                  var item = searchTodo[index];
                  return todoItem(
                    title: item.title!,
                    index: index,
                    completedStatus: getStatusAsBool(item.isCompleted),
                  );
                }),
              ),
            )
          ],
        ),
      ),
    );
  }

// section having a title with a button
  Padding titleWithButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 22.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text.rich(
            TextSpan(
              text: "Your list of ",
              style: TextStyle(
                fontSize: 20,
              ),
              children: [
                TextSpan(
                  text: "Todos",
                  style: TextStyle(
                    color: btnColor,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic,
                  ),
                )
              ],
            ),
          ),
          TextButton(
            onPressed: (() async {
              taskTextField(
                context: context,
                formKey: _todoItemKey,
                newItemController: _todoItemController,
                saveTodoItem: _saveTodoItem,
              );
              // await _helper.deleteTodoItem(null);
            }),
            style: TextButton.styleFrom(
              backgroundColor: Colors.green,
            ),
            child: const Text(
              "Add task",
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
    );
  }

// single todo item
  Padding todoItem(
      {required String title,
      required bool completedStatus,
      required int index}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 6),
      child: Container(
        decoration: BoxDecoration(
          boxShadow: const [
            BoxShadow(
              blurRadius: 10,
              offset: Offset(2, 2),
              color: Color.fromARGB(15, 0, 0, 0),
            ),
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
        ),
        child: ListTile(
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 15, vertical: 0.0),
          title: Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.w300,
              fontSize: 15,
              decoration: completedStatus ? TextDecoration.lineThrough : null,
            ),
          ),
          leading: completedStatus
              ? const Icon(Icons.check_box, color: Colors.green)
              : const Icon(Icons.check_box_outline_blank, color: Colors.green),
          horizontalTitleGap: 5,
          trailing: GestureDetector(
            onTap: () {
              _deleteTodoItem(todoList[index]);
            },
            child: Container(
              width: 25,
              height: 25,
              decoration: BoxDecoration(
                  color: Colors.deepOrange,
                  borderRadius: BorderRadius.circular(5)),
              child: const Center(
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                  size: 20,
                ),
              ),
            ),
          ),
          onTap: () {
            setState(() {
              completedStatus = !completedStatus;
              todoList[index].isCompleted = getStatusAsInt(completedStatus);
              updateTodoItem(todoList[index]);
            });
          },
          onLongPress: () {
            taskTextField(
              context: context,
              formKey: _todoItemKey,
              item: todoList[index],
              newItemController: _todoItemController,
              saveTodoItem: _saveTodoItem,
              updateItem: true,
            );
          },
        ),
      ),
    );
  }

// update a sinlge todo item on value change
  void updateTodoItem(Todo todo) async {
    await _helper.updateTodoItem(todo);
  }

// save todo item on the database
  void _saveTodoItem(context, [bool updateItem = false, Todo? item]) async {
    if (_todoItemController.text.isEmpty) {
      _todoItemKey.currentState!.validate();
    } else {
      if (item != null && updateItem == true) {
        item.title = _todoItemController.text;
        var result = await _helper.updateTodoItem(item);
        if (result != 0) {
          Navigator.pop(context);
          updateTodoList();
        }
      } else {
        todo.title = _todoItemController.text;
        var result = await _helper.insertTodoItem(todo);
        if (result != 0) {
          Navigator.pop(context);
          showSnackBar(context, "Task added successfully");
          updateTodoList();
        }
      }
    }
    _todoItemController.clear();
  }

// delete todo item from the database
  void _deleteTodoItem(Todo todo) async {
    int deleteStatus = 1;
    bool result = todoList.remove(todo);
    int duration = 4;
    if (result == true) {
      setState(() {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            backgroundColor: btnColor,
            content: const Text("Undo this action"),
            action: SnackBarAction(
              label: "Undo",
              onPressed: () {
                setState(() {
                  updateTodoList();
                  deleteStatus = 0;
                });
              },
              textColor: Colors.white,
            ),
            duration: Duration(seconds: duration),
          ),
        );
      });
    }
    // wait for "duration " time(seconds) before evaluating delete status
    Future.delayed(Duration(seconds: duration), () {
      if (deleteStatus != 0) {
        Future<int> res = _helper.deleteTodoItem(todo.id);
        res.then((value) {
          if (value != 0) {
            updateTodoList();
            showSnackBar(
              context,
              "Item deleted successfully, and cannot be restored!",
              2,
            );
          } else {
            showSnackBar(context, "Could not delete the item");
          }
        });
      }
    });
  }

  // update the list of Todos
  void updateTodoList() {
    var futureTodoList = _helper.getTodoItems(todo);
    futureTodoList.then((futureTodoList) {
      setState(() {
        todoList = futureTodoList.reversed.toList();
        searchTodo = todoList;
      });
    });
  }

// convert todo-item status from int to bool
// for convinience
  bool getStatusAsBool(int status) {
    switch (status) {
      case 0:
        isCompleted = false;
        break;
      case 1:
        isCompleted = true;
        break;
      default:
        isCompleted = false;
    }
    return isCompleted;
  }

// convert todo-item status from bool to int
// before saving it on the database
  int getStatusAsInt(bool isCompleted) {
    int status;
    switch (isCompleted) {
      case false:
        status = 0;
        break;
      case true:
        status = 1;
        break;
      default:
        status = 0;
    }
    return status;
  }
}
