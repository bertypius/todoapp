import 'package:flutter/material.dart';

import 'package:todoapp/resources/constants.dart';
import 'package:todoapp/models/onboard_data.dart';
import 'package:todoapp/screens/auth/wrapper.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({super.key});

  @override
  State<OnBoardingScreen> createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  final PageController _pageController = PageController();
  int currentPage = 0;
  // final _onboardInstance = OnBoardData("", "", "", "");

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  AnimatedContainer _buildDots(index) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 800),
      curve: Curves.easeInOut,
      height: currentPage == index ? 5.5 : 7.5,
      width: currentPage == index ? 15.0 : 7.5,
      margin: const EdgeInsets.symmetric(horizontal: 2.5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        boxShadow: [
          BoxShadow(blurRadius: 20, color: Colors.black.withOpacity(.13))
        ],
        color: currentPage == index ? Colors.green : Colors.white,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
        child: Container(
          color: btnColor,
          height: size.height,
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: bgColor,
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(size.height * 0.8),
                  ),
                ),
                height: size.height * 0.8,
                child: PageView.builder(
                  controller: _pageController,
                  onPageChanged: (value) {
                    setState(() {
                      currentPage = value;
                    });
                  },
                  itemCount: OnBoardData.listOfOnBoardData.length,
                  itemBuilder: (context, index) {
                    var data = OnBoardData.listOfOnBoardData[index];
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30.0),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 15,
                          ),
                          Text(
                            data.title!,
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium!
                                .copyWith(color: Colors.black),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            data.subtitle!,
                            style: const TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.5,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                  data.image!,
                                ),
                                fit: BoxFit.contain,
                                alignment: Alignment.center,
                              ),
                            ),
                          ),
                          Text(
                            data.description!,
                            style: const TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w200,
                                fontSize: 17,
                                height: 1.5),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 25.0, vertical: 20),
                  margin: const EdgeInsets.only(bottom: 20),
                  color: btnColor,
                  child: Column(
                    children: [
                      currentPage == OnBoardData.listOfOnBoardData.length - 1
                          ? btnGetStarted()
                          : Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                onBoardingNavBtn(
                                  ontap: () {
                                    _pageController.previousPage(
                                        duration:
                                            const Duration(microseconds: 5),
                                        curve: Curves.linearToEaseOut);
                                  },
                                  icon: Icons.arrow_back_sharp,
                                ),
                                Expanded(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: List.generate(
                                        OnBoardData.listOfOnBoardData.length,
                                        (index) => _buildDots(index)),
                                  ),
                                ),
                                onBoardingNavBtn(
                                  ontap: () {
                                    _pageController.nextPage(
                                        duration:
                                            const Duration(microseconds: 5),
                                        curve: Curves.linearToEaseOut);
                                  },
                                  icon: Icons.arrow_forward_sharp,
                                ),
                              ],
                            ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  GestureDetector onBoardingNavBtn({
    required VoidCallback ontap,
    required IconData icon,
  }) {
    return GestureDetector(
      onTap: ontap,
      child: AnimatedContainer(
        duration: const Duration(seconds: 1),
        curve: Curves.fastLinearToSlowEaseIn,
        width: 43,
        height: 43,
        decoration: BoxDecoration(
          color: Colors.white54,
          borderRadius: BorderRadius.circular(50),
          boxShadow: [
            BoxShadow(
              blurRadius: 22,
              offset: const Offset(0, 10),
              color: btnColor.withOpacity(0.22),
            ),
            BoxShadow(
              blurRadius: 22,
              offset: const Offset(-1, -1),
              color: Colors.black.withOpacity(.2),
            )
          ],
        ),
        child: Center(
          child: Icon(
            icon,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  btnGetStarted() {
    Size size = MediaQuery.of(context).size;
    return AnimatedContainer(
      duration: const Duration(milliseconds: 400),
      width: size.width * 0.6,
      child: TextButton(
        onPressed: (() {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (_) => const AuthScreenToggler(),
            ),
          );
        }),
        style: TextButton.styleFrom(
            backgroundColor: Colors.green,
            elevation: 30,
            minimumSize: Size(
              size.width * 0.6,
              size.width * 0.12,
            )),
        child: const Text(
          "Get Started",
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }
}
