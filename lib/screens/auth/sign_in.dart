import '../../resources/includes.dart';

class SignInScreen extends StatefulWidget {
  final VoidCallback toggleAuthScreen;
  const SignInScreen({super.key, required this.toggleAuthScreen});

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final _formKey = GlobalKey<FormState>();
  final _passwordController = TextEditingController();
  final _emailController = TextEditingController();
  final _user = User();
  final _login = Login();
  String? error = "";
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
          statusBarBrightness: Brightness.dark,
          statusBarColor: bgColor,
        ),
        elevation: 0,
        backgroundColor: bgColor,
        automaticallyImplyLeading: false,
      ),
      backgroundColor: bgColor,
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: size.width * .1),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // SizedBox(
                //   height: size.height * .13,
                // ),
                const Center(
                  child: Text(
                    "Hello!",
                    style: TextStyle(
                      color: Colors.green,
                      fontSize: 30,
                    ),
                  ),
                ),
                SizedBox(
                  height: size.height * .03,
                ),
                Center(
                  child: Text(
                    "Welcome back, we missed you !",
                    style: TextStyle(
                      color: btnColor,
                      fontSize: size.width * .058,
                      fontWeight: FontWeight.w100,
                    ),
                  ),
                ),
                SizedBox(
                  height: size.height * .12,
                ),
                formField(
                    size: size,
                    label: "Username",
                    hint: "enter your email",
                    controller: _emailController,
                    textInputType: TextInputType.emailAddress,
                    validator: (data) {
                      return _login.validateUserEmail(data);
                    }),
                SizedBox(height: size.height * 0.03),
                formField(
                    size: size,
                    label: "Password",
                    hint: "enter your password",
                    controller: _passwordController,
                    obscureText: true,
                    validator: (data) {
                      return _login.validateUserPassword(data);
                    }),
                SizedBox(height: size.height * 0.015),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: const [
                    TextButton(
                      onPressed: null,
                      child: Text(
                        "Forgot Password?",
                        style: TextStyle(
                          color: Colors.green,
                          fontSize: 17,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: size.height * 0.02),
                Center(
                  child: signUpBtn(
                    size: size,
                    label: "Continue",
                  ),
                ),
                SizedBox(height: size.height * 0.14),
                GestureDetector(
                  onTap: widget.toggleAuthScreen,
                  child: const Text.rich(
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w300),
                    TextSpan(text: "Do not have an account ? ", children: [
                      TextSpan(
                          text: " Sign up here",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, color: Colors.black))
                    ]),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  GestureDetector signUpBtn({required Size size, required String label}) {
    return GestureDetector(
      onTap: () {
        var result = _formKey.currentState!.validate();
        if (result) {
          _submitData(context);
        }
      },
      child: Container(
        height: size.height * 0.06,
        width: size.width * 0.8,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: btnColor,
          boxShadow: [
            BoxShadow(
              blurRadius: 30,
              offset: const Offset(0, 10),
              color: btnColor.withOpacity(0.15),
            ),
            BoxShadow(
              blurRadius: 22,
              offset: const Offset(-1, -1),
              color: Colors.black.withOpacity(.2),
            )
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              label,
              style: const TextStyle(color: Colors.white, fontSize: 20),
            ),
            const SizedBox(width: 5),
            const Icon(
              Icons.arrow_forward_sharp,
              color: Colors.white,
              size: 18,
            )
          ],
        ),
      ),
    );
  }

  Column formField(
      {required Size size,
      required String hint,
      required String label,
      required TextEditingController controller,
      required validator,
      TextInputType? textInputType,
      bool? obscureText}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.w500,
            color: Colors.black.withOpacity(.75),
          ),
          textAlign: TextAlign.left,
        ),
        SizedBox(
          height: size.height * .01,
        ),
        TextFormField(
          controller: controller,
          keyboardType: textInputType,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          obscureText: obscureText ?? false,
          decoration: InputDecoration(
            hintText: hint,
            hintStyle:
                const TextStyle(fontSize: 17, fontWeight: FontWeight.w300),
            errorStyle: const TextStyle(
              color: Colors.deepOrange,
              fontWeight: FontWeight.w300,
            ),
            fillColor: Colors.white,
            filled: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: BorderSide.none,
            ),
            contentPadding: const EdgeInsets.symmetric(
              vertical: 0,
              horizontal: 15,
            ),
          ),
          validator: ((value) {
            return validator(value);
          }),
        ),
      ],
    );
  }

  void _submitData(BuildContext context) {
    _user.email = _emailController.text.trim();
    _user.password = _passwordController.text.trim();
    var result = _login.verifyUser(_user);
    result.then(
      (result) {
        if (result.isNotEmpty) {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => HomeScreen(_login.username)));
        } else {
          error = _login.validateUserPasswordMatch();
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                contentPadding: const EdgeInsets.symmetric(vertical: 5),
                title: const Text(
                  textAlign: TextAlign.center,
                  "Error !",
                  style: TextStyle(
                      color: Colors.deepOrange,
                      fontSize: 20,
                      fontWeight: FontWeight.w300),
                ),
                titlePadding: const EdgeInsets.only(top: 10),
                content: Text(
                  textAlign: TextAlign.center,
                  error!,
                  style:
                      const TextStyle(fontWeight: FontWeight.w300, height: 1.3),
                ),
                actionsAlignment: MainAxisAlignment.center,
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    style: TextButton.styleFrom(
                      minimumSize: const Size(90, 31),
                      backgroundColor: btnColor,
                    ),
                    child: const Text(
                      "Retry",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              );
            },
          );
        }
      },
    );
  }
}
