import '../../resources/includes.dart';

AppBar appBar(String username) {
  return AppBar(
    systemOverlayStyle: const SystemUiOverlayStyle(
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness: Brightness.dark,
      statusBarColor: btnColor,
    ),
    elevation: 0,
    automaticallyImplyLeading: false,
    backgroundColor: Colors.orange,
    title: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Icon(Icons.menu, color: Colors.white),
        Text(
          username,
          style: const TextStyle(fontWeight: FontWeight.w300),
        ),
        const CircleAvatar(
          backgroundColor: btnColor,
          backgroundImage: AssetImage("assets/images/profile/profile.jpg"),
        )
      ],
    ),
  );
}
