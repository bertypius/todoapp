import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:todoapp/resources/constants.dart';
import 'package:todoapp/models/user.dart';
import 'package:todoapp/utils/database_helper.dart';

import '../home/home.dart';

class SignUpScreen extends StatefulWidget {
  final VoidCallback toggleAuthScreen;
  const SignUpScreen({super.key, required this.toggleAuthScreen});

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _formKey = GlobalKey<FormState>();
  final _helper = DatabaseHelper();
  final _passwordController = TextEditingController();
  final _usernameController = TextEditingController();
  final _emailController = TextEditingController();
  final _user = User();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
          statusBarBrightness: Brightness.dark,
          statusBarColor: bgColor,
        ),
        elevation: 0,
        backgroundColor: bgColor,
        automaticallyImplyLeading: false,
      ),
      backgroundColor: bgColor,
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: size.width * .1),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Center(
                  child: Text(
                    "Welcome back !",
                    style: TextStyle(
                      color: Colors.green,
                      fontSize: 30,
                    ),
                  ),
                ),
                SizedBox(
                  height: size.height * .02,
                ),
                const Text(
                  "Create new account here",
                  style: TextStyle(
                    color: btnColor,
                    fontSize: 25,
                    fontWeight: FontWeight.w100,
                  ),
                ),
                SizedBox(
                  height: size.height * .08,
                ),
                formField(
                    size: size,
                    label: "Username",
                    hint: "enter your name",
                    controller: _usernameController,
                    validator: (data) {
                      return User.validateUserName(data);
                    }),
                SizedBox(height: size.height * 0.03),
                formField(
                  size: size,
                  label: "Email address",
                  hint: "enter your email",
                  controller: _emailController,
                  textInputType: TextInputType.emailAddress,
                  validator: (data) {
                    return User.validateUserEmail(data);
                  },
                ),
                SizedBox(height: size.height * 0.03),
                formField(
                    size: size,
                    label: "Password",
                    hint: "create your password",
                    controller: _passwordController,
                    validator: (data) {
                      return User.validateUserPassword(data);
                    }),
                SizedBox(height: size.height * 0.04),
                Center(
                  child: signUpBtn(
                    size: size,
                  ),
                ),
                SizedBox(height: size.height * 0.1),
                GestureDetector(
                  onTap: widget.toggleAuthScreen,
                  child: const Text.rich(
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w300),
                    TextSpan(text: "Already have an account ? ", children: [
                      TextSpan(
                          text: " Sign in here",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, color: Colors.black))
                    ]),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  GestureDetector signUpBtn({required Size size}) {
    return GestureDetector(
      onTap: () {
        var result = _formKey.currentState!.validate();
        if (result) {
          _saveData(context);
        }
      },
      child: Container(
        height: size.height * 0.06,
        width: size.width * 0.6,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: btnColor,
          boxShadow: [
            BoxShadow(
              blurRadius: 22,
              offset: const Offset(0, 10),
              color: btnColor.withOpacity(0.15),
            ),
            BoxShadow(
              blurRadius: 22,
              offset: const Offset(-1, -1),
              color: Colors.black.withOpacity(.2),
            )
          ],
        ),
        child: const Center(
            child: Text(
          "Create Account",
          style: TextStyle(color: Colors.white, fontSize: 20),
        )),
      ),
    );
  }

  Column formField({
    required Size size,
    required String hint,
    required String label,
    required TextEditingController controller,
    required validator,
    TextInputType? textInputType,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.w500,
            color: Colors.black.withOpacity(.75),
          ),
          textAlign: TextAlign.left,
        ),
        SizedBox(
          height: size.height * .01,
        ),
        TextFormField(
          controller: controller,
          keyboardType: textInputType,
          decoration: InputDecoration(
            hintText: hint,
            hintStyle:
                const TextStyle(fontSize: 17, fontWeight: FontWeight.w300),
            errorStyle: const TextStyle(
                color: Colors.deepOrange, fontWeight: FontWeight.w300),
            fillColor: Colors.white,
            filled: true,
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30),
                borderSide: BorderSide.none),
            contentPadding:
                const EdgeInsets.symmetric(vertical: 0, horizontal: 15),
          ),
          validator: ((value) {
            return validator(value);
          }),
        ),
      ],
    );
  }

  void _saveData(BuildContext context) {
    _user.name = _usernameController.text.trim();
    _user.email = _emailController.text.trim();
    _user.password = _passwordController.text.trim();
    debugPrint(_user.password);
    Future<int> result = _helper.createUser(_user);
    result.then((value) {
      if (value != 0) {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => HomeScreen(_user.name!)));
      } else {
        return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              actionsAlignment: MainAxisAlignment.center,
              contentPadding: const EdgeInsets.symmetric(vertical: 5),
              title: const Text(
                textAlign: TextAlign.center,
                "Error !",
                style: TextStyle(
                  color: Colors.deepOrange,
                  fontSize: 23,
                ),
              ),
              titlePadding: const EdgeInsets.only(top: 10),
              content: const Text(
                textAlign: TextAlign.center,
                "Could not save user information",
                style: TextStyle(
                  fontWeight: FontWeight.w300,
                ),
              ),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  style: TextButton.styleFrom(
                    backgroundColor: btnColor,
                    minimumSize: const Size(100, 31),
                    padding: const EdgeInsets.symmetric(vertical: 5),
                  ),
                  child: const Text(
                    "Exit",
                    style: TextStyle(
                        color: Colors.white, fontSize: 17, letterSpacing: 1),
                  ),
                ),
              ],
            );
          },
        );
      }
    });
  }
}
