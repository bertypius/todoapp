import 'package:flutter/material.dart';
import 'package:todoapp/screens/auth/sign_in.dart';
import 'package:todoapp/screens/auth/sign_up.dart';
import '../../widgets/willpopscope.dart';

class AuthScreenToggler extends StatefulWidget {
  const AuthScreenToggler({super.key});

  @override
  State<AuthScreenToggler> createState() => _AuthScreenTogglerState();
}

class _AuthScreenTogglerState extends State<AuthScreenToggler> {
  bool showLoginScreen = true;
  void _toggleAuthScreen() async {
    setState(() {
      showLoginScreen = !showLoginScreen;
    });
  }

  @override
  Widget build(BuildContext context) {
    return CustomWillPopScope(
      child: showLoginScreen
          ? SignInScreen(toggleAuthScreen: _toggleAuthScreen)
          : SignUpScreen(toggleAuthScreen: _toggleAuthScreen),
    );
  }
}
