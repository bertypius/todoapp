class Todo {
  int? _id;
  String? _title;
  int _status = 0;
  Todo();
  Todo.withParam(this._id, this._title, this._status);

  // Getters and Setters
  int? get id => _id;
  String? get title => _title;
  int get isCompleted => _status;
  set title(title) => _title = title;
  set isCompleted(int isCompleted) {
    if (isCompleted == 0 || isCompleted == 1) {
      _status = isCompleted;
    }
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      "id": _id,
      "title": _title,
      "status": isCompleted,
    };
    return map;
  }

  Todo.fromMap(Map<String, dynamic> map) {
    _id = map["id"];
    _title = map["title"];
    _status = map["status"];
  }
}
