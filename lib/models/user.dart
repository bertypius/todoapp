class User {
  int? _id;
  String? _name;
  String? _email;
  String? _password;

  User();
  User.named(this._id, this._name, this._email, this._password);

// Getters and Setters
  int? get id => _id;
  String? get name => _name;
  String? get email => _email;
  String? get password => _password;
  set name(name) => _name = name;
  set email(email) => _email = email;
  set password(password) => _password = password;

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["email"] = _email;
    map["password"] = _password;
    return map;
  }

  User.fromMap(Map<String, dynamic> map) {
    _id = map["id"];
    _name = map["name"];
    _email = map["email"];
    _password = map["password"];
  }

  static String? validateUserName(String? data) {
    if (data!.isEmpty) {
      return "This field is required";
    } else if (!RegExp(r'(^[a-z A-Z]+$)').hasMatch(data)) {
      return "Enter a valid user name";
    }
    return null;
  }

  static String? validateUserEmail(String? data) {
    if (data!.isEmpty) {
      return "This field is required";
    } else if (!RegExp(r'(^[\w-\.]+@([\w-]+\.)+[\w]{2,5}$)').hasMatch(data)) {
      return "Enter a valid email address";
    }
    return null;
  }

  static String? validateUserPassword(String? data) {
    if (data!.isEmpty) {
      return "This field is required";
    } else if (data.length < 6) {
      return "Your password should be at least 6 characters";
    } else {
      return null;
    }
  }
}
