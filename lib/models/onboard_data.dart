class OnBoardData {
  String? image;
  String? title;
  String? subtitle;
  String? description;
  OnBoardData.named();
  OnBoardData(this.title, this.subtitle, this.description, this.image);

  static List<OnBoardData> listOfOnBoardData = [
    OnBoardData(
        "First screen",
        "This is the first onboarding screen",
        "Consequuntur laudantium et nostrud sit ullam vero sint sit facere eos libero amet laudantium et nostrud",
        "assets/images/onboarding/onboard3.png"),
    OnBoardData(
        "Second screen",
        "This is the second onboarding screen",
        "Consequuntur laudantium et nostrud sit ullam vero sint sit facere eos libero amet laudantium et nostrud",
        "assets/images/onboarding/onboard5.png"),
    OnBoardData(
        "Third screen",
        "This is the third onboarding screen",
        "Consequuntur laudantium et nostrud sit ullam vero sint sit facere eos libero amet laudantium et nostrud",
        "assets/images/onboarding/onboard5.png"),
  ];
}
