import 'resources/includes.dart';

bool? seenOnboarding;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: bgColor,
    statusBarIconBrightness: Brightness.dark,
  ));
  SharedPreferences pref = await SharedPreferences.getInstance();
  seenOnboarding = pref.getBool("seenOnboarding") ?? false;
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Todo Application",
      home: seenOnboarding == true
          ? const AuthScreenToggler()
          : const OnBoardingScreen(),
      // home: OnBoardingScreen(),
    );
  }
}
